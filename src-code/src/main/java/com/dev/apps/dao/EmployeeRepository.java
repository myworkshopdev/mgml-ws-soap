package com.dev.apps.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.dev.apps.entity.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
	@Query("SELECT e FROM Employee e WHERE e.empId = :id")
	public Employee getEmployeeById(@Param("id") Long id);

	@Query("SELECT e FROM Employee e")
	public List<Employee> getAllEmployee();

}
