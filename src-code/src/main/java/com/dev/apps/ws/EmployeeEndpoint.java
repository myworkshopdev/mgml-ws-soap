package com.dev.apps.ws;

import java.util.List;

import javax.xml.bind.JAXBElement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.dev.apps.entity.Employee;
import com.dev.apps.schema.EmployeeDetails;
import com.dev.apps.schema.EmployeeRequest;
import com.dev.apps.schema.EmployeeResponse;
import com.dev.apps.schema.ObjectFactory;
import com.dev.apps.service.EmployeeServiceImpl;

@Endpoint
public class EmployeeEndpoint<employeeResponse> {
	private static final String NAMESPACE_URI = "http://www.example.org/Employee";

	@Autowired
	public EmployeeServiceImpl employeeServiceImpl;

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "employeeRequest")
	@ResponsePayload
	public JAXBElement<EmployeeResponse> getEmployee(@RequestPayload JAXBElement<EmployeeRequest> request) {
		ObjectFactory objFactory = new ObjectFactory();
		EmployeeResponse empResFactory = objFactory.createEmployeeResponse();
		JAXBElement<EmployeeResponse> empResponse = objFactory.createEmployeeResponse(empResFactory);

		try {
			int action = request.getValue().getAction();
			EmployeeResponse empResObj = new EmployeeResponse();

			switch (action) {
			case 1:
				List<Employee> allEmployee = employeeServiceImpl.getAllEmployee();

				if (allEmployee != null) {
					for (Employee emp : allEmployee) {
						EmployeeDetails empDetail = new EmployeeDetails();
						empDetail.setId(emp.getEmpId().intValue());
						empDetail.setAlias(emp.getEmpAlias());
						empDetail.setDni(emp.getEmpDni());
						empDetail.setEmail(emp.getEmpEmail());
						empDetail.setName(emp.getEmpName());
						empDetail.setPassword(emp.getEmpPassword());

						empResObj.getEmployeeDetails().add(empDetail);
					}
				}

				empResponse.setValue(empResObj);

				break;
			case 2:
				Employee emp = employeeServiceImpl.getEmployeeById(new Long(request.getValue().getId()));

				if (emp != null) {
					EmployeeDetails empDetail = new EmployeeDetails();
					empDetail.setId(emp.getEmpId().intValue());
					empDetail.setAlias(emp.getEmpAlias());
					empDetail.setDni(emp.getEmpDni());
					empDetail.setEmail(emp.getEmpEmail());
					empDetail.setName(emp.getEmpName());
					empDetail.setPassword(emp.getEmpPassword());

					empResObj.getEmployeeDetails().add(empDetail);
				}

				empResponse.setValue(empResObj);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return empResponse;
	}
}
