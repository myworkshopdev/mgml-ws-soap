package com.dev.apps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SrcCodeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SrcCodeApplication.class, args);
	}

}
