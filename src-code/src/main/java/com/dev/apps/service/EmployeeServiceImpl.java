package com.dev.apps.service;

import java.util.List;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dev.apps.dao.EmployeeRepository;
import com.dev.apps.entity.Employee;

@Service("EmployeeService")
@Transactional
public class EmployeeServiceImpl implements EmployeeService {
	private static final Logger logger = LogManager.getLogger(EmployeeServiceImpl.class);
	
	@Autowired
	public EmployeeRepository repository;

	@Override
	public Employee getEmployeeById(Long id) {
		return repository.getEmployeeById(id);
	}

	@Override
	public List<Employee> getAllEmployee() {
		logger.info("Total de elementos de la lista: " + repository.getAllEmployee().size());
		return repository.getAllEmployee();
	}

}
