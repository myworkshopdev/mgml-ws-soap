package com.dev.apps.service;

import java.util.List;

import com.dev.apps.entity.Employee;

public interface EmployeeService {
	public Employee getEmployeeById(Long id);
	public List<Employee> getAllEmployee();
}
