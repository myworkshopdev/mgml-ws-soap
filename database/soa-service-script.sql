-- creacion de base de datos
CREATE DATABASE soaservice WITH OWNER = troll ENCODING = 'UTF8' CONNECTION LIMIT = -1;

-- creacion de tablas
CREATE TABLE employee ( 
  emp_id       int8 NOT NULL,
  emp_name     VARCHAR(200) NOT NULL,
  emp_alias    VARCHAR(100),
  emp_password VARCHAR(50),
  emp_dni      VARCHAR(11),
  emp_email    VARCHAR(100),  
  CONSTRAINT emp_pk PRIMARY KEY (emp_id)
);

CREATE TABLE task (
  task_id            int8 NOT NULL,
  task_name          VARCHAR(250) NOT NULL,
  emp_id             int8,
  task_dateini       DATE,
  task_date_end      DATE,
  task_status        int8,   -- 0 No iniciada, 1 En proceso, 2 Finalizada  
  CONSTRAINT task_pk PRIMARY KEY (task_id)
);

-- Creacion de seccuencias 
CREATE SEQUENCE emp_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 1
	CACHE 1
	NO CYCLE;
 
 CREATE SEQUENCE task_seq
 	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 1
	CACHE 1
	NO CYCLE;

--Insert Table employee
insert into employee(emp_id, emp_name, emp_alias, emp_password, emp_dni, emp_email)
       values (nextval('emp_seq'),'Luis Santiago Salas','lsalas01','12345','12345678901','lsa01@tot.com');
insert into employee(emp_id, emp_name, emp_alias, emp_password, emp_dni, emp_email)
       values (nextval('emp_seq'),'Liliana Jaramillo Torres','ljaramillo01','54321','54321678901','ljara01@jata.com');       
COMMIT;

--Insert Table task
insert into task(task_id, task_name, emp_id, task_dateini, task_date_end, task_status)
       values (nextval('task_seq'),'Instalar Windows 8',1,now(),CURRENT_DATE + 3,0);       
insert into task(task_id, task_name, emp_id, task_dateini, task_date_end, task_status)
       values (nextval('task_seq'),'Instalar Office 2013',1,CURRENT_DATE+4,CURRENT_DATE+7,0);    
insert into task(task_id, task_name, emp_id, task_dateini, task_date_end, task_status)
       values (nextval('task_seq'),'Instalar Oracle 11g',2,now(),CURRENT_DATE+2,0);        
COMMIT;

